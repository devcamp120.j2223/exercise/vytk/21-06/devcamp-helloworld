const gDevcampReact = {
  title: 'Chào mừng đến với Devcamp React',
  image: 'https://reactjs.org/logo-og.png',
  benefits: ['Blazing Fast', 'Seo Friendly', 'Reusability', 'Easy Testing', 'Dom Virtuality', 'Efficient Debugging'],
  studyingStudents: 20,
  totalStudents: 100
}

const gTyLeSinhVien=()=>gDevcampReact.studyingStudents/gDevcampReact.totalStudents*100;

function App() {
  return (
    <div className="App">
      <h1>{gDevcampReact.title}</h1>
      <img src={gDevcampReact.image} width="500px"/>
      <p>Tỷ lệ sinh viên đang theo học: {gTyLeSinhVien()}%</p>
      <p>{gTyLeSinhVien() >=15? "Sinh viên theo học nhiều" : "Sinh viên theo học ít"}</p>
      <ul>
        {
          gDevcampReact.benefits.map((value,index)=>{
            return <li key={index}>{value}</li>
          })
        }
      </ul>
    </div>
  );
}

export default App;
